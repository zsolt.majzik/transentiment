.DEFAULT_GOAL := help
.PHONY: test help

sources = transentiment tests

help:  ## these help instructions
	@sed -rn 's/^([a-zA-Z_-]+):.*?## (.*)$$/"\1" "\2"/p' < $(MAKEFILE_LIST)|xargs printf "make %-20s# %s\n"

hidden: # example undocumented, for internal usage only
	@true

pydoc: ## Run a pydoc server and open the browser
	poetry run python -m pydoc -b

install: ## Run `poetry install`
	poetry install

showdeps: ## run poetry to show deps
	@echo "CURRENT:"
	poetry show --tree
	@echo
	@echo "LATEST:"
	poetry show --latest

lint: ## Runs bandit and black in check mode
	poetry run black $(sources) --check --diff
	@echo '-------------------------------'
	poetry run ruff $(sources)
	@echo '-------------------------------'

format: ## Formats you code with Black
	poetry run black $(sources)
	poetry run ruff --fix $(sources)

mypy: ## Runs mypy
	poetry run mypy $(sources)

test: hidden ## run pytest with coverage
	poetry run pytest -v --cov py.test --ignore=extern

build: install lint test ## run `poetry build` to build source distribution and wheel
	poetry build

jupyter: ## run the jupyter-lab server
	poetry run jupyter-lab
run: ## run `poetry run diyml`
	poetry run diyml

clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]'`
	rm -f `find . -type f -name '*~'`
	rm -f `find . -type f -name '.*~'`
	rm -rf .cache
	rm -rf .pytest_cache
	rm -rf .ruff_cache
	rm -rf .mypy_cache
	rm -rf htmlcov
	rm -rf *.egg-info
	rm -f .coverage
	rm -rf .ipynb_checkpoints
	rm -f .coverage.*
	rm -rf coverage.xml
