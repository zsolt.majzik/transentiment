# TransformerClassifier (on IMDB Dataset)

## Introduction

The `TransformerClassifier` is designed for sentiment analysis (currently only) on the IMDB dataset. This guide will help you understand its parameters and how to use it.

## Parameters

- **batch_size**: Size of each training batch.
- **maxlen**: Maximum sequence length. Represents the maximum number of tokens in a sequence. Text sequences longer than this value will be truncated, and those shorter will be padded.
- **vocab_size**: The parameter represents the total number of unique tokens (e.g., words or subwords) the model can recognize and utilize. This parameter determines the size of the vocabulary, which in turn impacts the embedding layer of the model.
- **embed_dim**: The parameter represents the size (or dimensionality) of the embedding vectors used to represent each token in the vocabulary. Embeddings are dense vector representations where each token (e.g., word or subword) is mapped to a vector in continuous space.
- **num_heads**: the parameter refers to the number of independent attention mechanisms (or "heads") used within the multi-head attention layer.
- **ff_dim**: Hidden layer size inside the transformer.
- **classifier_dims**: these parameters are representing the dimensions of the hidden layers in the classifier network that follows the Transformer block in the model.
- **lr**: Learning rate.
- **num_samples**: Number of hyperparameter tuning trials.
- **num_epochs**: Total training epochs.
- **tb_save_dir**: Directory for saving TensorBoard logs.
