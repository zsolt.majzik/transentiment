import os
from datetime import datetime

import hydra
from hydra.utils import to_absolute_path
from lightning.pytorch.loggers import MLFlowLogger
from omegaconf import DictConfig
from pytorch_lightning import Trainer, callbacks

from transentiment.data.datamodule import IMDBDataModule
from transentiment.model.classifier import TransformerClassifier


@hydra.main(config_path='conf', config_name='config')
def main(cfg: DictConfig):
    checkpoint_path = to_absolute_path(cfg.paths.checkpoint_path)

    # Create a PyTorch Lightning trainer with the generation callback
    root_dir = os.path.join(checkpoint_path, 'GraphLevel' + cfg.model_name)
    os.makedirs(root_dir, exist_ok=True)

    # Initialize the model
    model = TransformerClassifier(**cfg.model)

    # Initialize data module
    datamodule = IMDBDataModule(**cfg.datamodule)
    current_datetime = datetime.now()
    log_time = current_datetime.strftime('%Y-%m-%d_%H:%M:%S')

    logger = None
    # MLFlow
    if cfg.mlflow.enabled:
        logger = MLFlowLogger(
            experiment_name=cfg.mlflow.experiment_name,
            run_name=f'train-{log_time}',
            tracking_uri=cfg.mlflow.tracking_uri,
            log_model=cfg.mlflow.log_model,
        )

    # log all Hydra params with mlflow
    for p_name, p_val in cfg.items():
        logger.experiment.log_param(run_id=logger.run_id, key=p_name, value=p_val)

    # Initialize trainer
    trainer = Trainer(
        default_root_dir=root_dir,
        logger=logger,
        callbacks=[callbacks.ModelCheckpoint(dirpath=root_dir, save_weights_only=True, mode='max', monitor='val_acc')],
        **cfg.trainer,
    )

    # Start training
    trainer.fit(model, datamodule=datamodule)


if __name__ == '__main__':
    main()
