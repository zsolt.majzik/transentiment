import torch
import torch.nn as nn
from torch import Tensor


class TokenAndPositionEmbedding(nn.Module):
    def __init__(self, maxlen: int, vocab_size: int, embed_dim: int):
        """Token And position embedding.

        Args:
            maxlen: The maximum length of the sequences for which position embeddings are generated.
                This determines the size of the position embedding lookup table.
            vocab_size: The size of the vocabulary. Determines the size of the token embedding lookup table.
            embed_dim: The embedding dimension for both token and position embeddings.
        """
        super().__init__()
        self.token_emb = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embed_dim)
        self.pos_emb = nn.Embedding(num_embeddings=maxlen, embedding_dim=embed_dim)

    def forward(self, x: Tensor) -> Tensor:
        maxlen = x.size(1)
        positions = torch.arange(0, maxlen, dtype=torch.long).unsqueeze(0).expand(x.size(0), -1).to(x.device)

        return self.token_emb(x) + self.pos_emb(positions)
