import torch.nn as nn
from torch.nn import Dropout, LayerNorm, Linear, MultiheadAttention


class FFN(nn.Module):
    def __init__(self, input_dim, hidden_dims, output_dim):
        """Feed Forward Network with N hidden layers.

        Args:
            input_dim: input (feature) dimension
            hidden_dims: A list containing the sizes of hidden layers.
                The length of the list determines the number of hidden layers.
                E.g., [512, 256] would create two hidden layers with sizes 512 and 256 respectively.
            output_dim: output dimension
        """
        super().__init__()

        layers = []
        prev_dim = input_dim

        for hidden_dim in hidden_dims:
            layers.append(Linear(prev_dim, hidden_dim))
            layers.append(nn.ReLU())
            prev_dim = hidden_dim

        layers.append(Linear(prev_dim, output_dim))

        self.network = nn.Sequential(*layers)

    def forward(self, x):
        return self.network(x)


class TransformerBlock(nn.Module):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1, eps=1e-6) -> None:
        """TransformerBlock

        The transformer block:
        0. Input embeddings
            |
            V
        1. MultiheadAttention   (Attention layer, "Attention Is All You Need")
            |
            V
        2. Dropout              (Attention dropout, prevent heavy dependence
            |                   on certain attention scores.)
            V
        3. LayerNorm            (Batch normalization
            |                   + residual connection from the input)
            V
        4. FFN                  (Feed-Forward Network)
            |
            V
        5. Dropout              (Position-wise FFN Dropout, reduce overfitting.)
            |
            V
        6. LayerNorm            (Batch normalization + residual connection from
                                the normalized attention mechanism)


        Args:
            embed_dim: The embedding dimension of the input and output. It's also the dimension
                that the multihead attention mechanism operates upon.
            num_heads: The number of attention heads in the multihead attention mechanism.
            ff_dim: The size of the hidden layer within the feed-forward network (FFN).
            rate: Dropout rate to be used after the multihead attention and the FFN.
            eps:  positive number added to the variance to avoid division by zero.
        """
        super().__init__()

        self.att = MultiheadAttention(embed_dim=embed_dim, num_heads=num_heads)

        self.ffn = FFN(embed_dim, [ff_dim], embed_dim)

        self.att = MultiheadAttention(embed_dim=embed_dim, num_heads=num_heads)
        self.layernorm1 = LayerNorm(normalized_shape=embed_dim, eps=eps)
        self.layernorm2 = LayerNorm(normalized_shape=embed_dim, eps=eps)
        self.dropout1 = Dropout(rate)
        self.dropout2 = Dropout(rate)

    def forward(self, inputs):
        attn_output, _ = self.att(inputs, inputs, inputs)
        attn_output = self.dropout1(attn_output)
        # attention residual (skip) connection:
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output)

        # FFN residual (skip) Connection
        return self.layernorm2(out1 + ffn_output)
