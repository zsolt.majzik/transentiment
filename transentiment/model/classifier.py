import pytorch_lightning as pl
import torch
import torch.nn.functional as F
from torch import nn
from torch.optim.lr_scheduler import ReduceLROnPlateau

from transentiment.model.token import TokenAndPositionEmbedding
from transentiment.model.transformer import FFN, TransformerBlock


class TransformerClassifier(pl.LightningModule):
    def __init__(
        self,
        maxlen: int,
        vocab_size: int,
        embed_dim: int,
        num_heads: int,
        ff_dim: int,
        classifier_dims: list,
        learning_rate: float = 1e-3,
    ):
        """TransformerClassifier module.

        Args:
            maxlen: Maximum length of the input sequence (used for positional encoding).
            vocab_size: Size of the vocabulary, i.e., number of unique tokens.
            embed_dim: Dimension of the embedding vectors.
            num_heads: Number of attention heads for the multi-head attention mechanism.
            ff_dim: Hidden layer size in the feed-forward network inside the transformer block.
            classifier_dims: A list containing the sizes of hidden layers in the classifier.
            learning_rate: Learning rate for the optimizer.
        """
        super().__init__()

        # Token and Position Embedding
        self.embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)

        # Transformer Block
        self.transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)

        # Classifier
        self.classifier = FFN(embed_dim, classifier_dims, 2)
        self.softmax = nn.Softmax(dim=1)

        # Learning rate
        self.learning_rate = learning_rate

    def forward(self, x):
        x = self.embedding_layer(x)
        x = self.transformer_block(x)
        # perform average pooling along the sequence length
        x = x.mean(dim=1)
        x = self.classifier(x)
        x = self.softmax(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, y)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate, weight_decay=1e-5)
        scheduler = {
            'scheduler': ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, verbose=True),
            'monitor': 'val_loss',
            'interval': 'epoch',
            'frequency': 1,
            'strict': True,
        }
        return {'optimizer': optimizer, 'lr_scheduler': scheduler, 'monitor': 'val_loss'}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, y)
        _, preds = torch.max(y_hat, dim=1)  # Get the predicted classes
        acc = (preds == y).float().mean()  # Compute the accuracy
        self.log('val_loss', loss, on_epoch=True, prog_bar=True, logger=True)
        self.log('val_acc', acc, on_epoch=True, prog_bar=True, logger=True)
