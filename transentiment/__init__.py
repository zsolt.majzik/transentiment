"""transentiment: Text Classification with Transformers"""

__author__ = 'Zsolt Majzik <zsolt.majzik@gmail.com>'
__version__ = '0.2'
__version_info__ = tuple([int(num) for num in __version__.split('.')])
