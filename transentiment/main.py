# from transentiment.train import train_transformer
# from ray.tune.schedulers import ASHAScheduler
# from ray import tune
# from functools import partial

# def main(num_samples=10, num_epochs=10, gpus_per_trial=1):

#     config = {
#         "maxlen": tune.choice([100, 150, 200]),
#         "vocab_size": tune.choice([15000, 20000, 25000]),
#         "embed_dim": tune.choice([32, 64, 128]),
#         "num_heads": tune.choice([2, 4, 8]),
#         "ff_dim": tune.choice([32, 64]),
#         "classifier_dims": tune.choice([[20], [40, 20], [60, 30, 15]]),
#         "lr": tune.loguniform(1e-4, 1e-1)
#     }

#     scheduler = ASHAScheduler(
#         metric="val_loss",
#         mode="min",
#         max_t=num_epochs,
#         grace_period=1,
#         reduction_factor=2
#     )

#     reporter = tune.CLIReporter(
#         metric_columns=["val_loss", "val_accuracy", "training_iteration"]
#     )

#     result = tune.run(
#         partial(train_transformer, data_dir=data_dir, num_epochs=num_epochs),
#         resources_per_trial={"cpu": 2, "gpu": gpus_per_trial},
#         config=config,
#         num_samples=num_samples,
#         scheduler=scheduler,
#         progress_reporter=reporter
#     )

#     best_trial = result.get_best_trial("val_loss", "min", "last")
#     print("Best trial config: {}".format(best_trial.config))
#     print("Best trial final validation loss: {}".format(best_trial.last_result["val_loss"]))
#     print("Best trial final validation accuracy: {}".format(best_trial.last_result["val_accuracy"]))


# if __name__ == "__main__":
#     main(num_samples=10, num_epochs=10, gpus_per_trial=1)
