import torch
from pytorch_lightning import LightningDataModule
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import DataLoader, random_split
from torchtext.data.utils import get_tokenizer
from torchtext.datasets import IMDB
from torchtext.vocab import build_vocab_from_iterator


class IMDBDataModule(LightningDataModule):
    def __init__(self, val_split: float = 0.2, vocab_size: int = 20000, maxlen: int = 200, batch_size: int = 32):
        """Initialize the IMDB data module with basic configurations."""
        super().__init__()
        self.val_split = val_split
        self.vocab_size = vocab_size
        self.maxlen = maxlen
        self.batch_size = batch_size
        self.tokenizer = get_tokenizer('basic_english')

    def tokenizer_fn(self, iterator):
        """Return an iterator after tokenizing the text."""
        return (self.tokenizer(text) for _, text in iterator)

    def prepare_data(self):
        """Download the IMDB data and build vocabulary."""
        # Download and tokenize
        self.train_iter, self.test_iter = IMDB()

        # Build Vocabulary
        self.vocab = build_vocab_from_iterator(
            self.tokenizer_fn(self.train_iter), specials=['<PAD>', '<UNK>'], max_tokens=self.vocab_size - 2
        )
        self.vocab.set_default_index(self.vocab['<UNK>'])

    def setup(self, stage=None):
        """Tokenize and split the IMDB dataset into train, val, and test."""
        # Convert text to numeric ids and split into train and val
        self.train_data = [
            (torch.tensor([self.vocab[token] for token in self.tokenizer(text)]), label - 1)
            for label, text in self.train_iter
        ]

        self.test_data = [
            (torch.tensor([self.vocab[token] for token in self.tokenizer(text)]), label - 1)
            for label, text in self.test_iter
        ]

        train_len = int((1.0 - self.val_split) * len(self.train_data))
        self.train_data, self.val_data = random_split(self.train_data, [train_len, len(self.train_data) - train_len])

    def collate_fn(self, batch):
        """Pad the sequences in a batch to a maximum length."""
        texts, labels = zip(*batch)
        texts = pad_sequence(texts, batch_first=True, padding_value=self.vocab['<PAD>'])[:, : self.maxlen]
        return texts, torch.tensor(labels, dtype=torch.int64)

    def train_dataloader(self):
        """Return DataLoader for training."""
        return DataLoader(
            self.train_data, batch_size=self.batch_size, shuffle=True, collate_fn=self.collate_fn, num_workers=4
        )

    def val_dataloader(self):
        """Return DataLoader for validation."""
        return DataLoader(self.val_data, batch_size=self.batch_size, collate_fn=self.collate_fn, num_workers=4)

    def test_dataloader(self):
        """Return DataLoader for testing."""
        return DataLoader(self.test_data, batch_size=self.batch_size, collate_fn=self.collate_fn, num_workers=4)
